<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Userinfo extends Model
{
    use HasFactory;
    protected $table = 'userinfo'; 
    protected $fillable = [
        'username', 
        'firstname', 
        'lastname', 
        'email', 
        'department', 
        'company', 
        'workphone', 
        'homephone', 
        'mobilephone', 
        'address', 'city', 
        'state', 
        'country', 
        'zip', 
        'notes', 
        'changeuserinfo', 
        'portalloginpassword', 
        'enableportallogin', 
        'creationdate', 
        'creationby', 
        'updatedate', 
        'updateby'
    ];
    
}

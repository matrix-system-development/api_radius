<?php

namespace App\Http\Controllers;

use App\Models\{Userinfo, RadUserGroup, Radcheck, RadReply};
use App\Traits\{CheckerTrait,ValidatorTrait};
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    const block_priority = -999;
    const unblock_priority = 0;
    const groupname_blocked = 'daloRADIUS-Disabled-Users';

    //after huawei
    const attribute = 'Filter-Id';
    const value = 'nomoney';

    
    use ValidatorTrait, CheckerTrait;

    public function CheckRadusergroupUser(Request $request)
    {
        // ../api/rad-checkradusergroup
        // return search result of username in radusergroup table
        $query = DB::table('radusergroup')->where('username', $request['username'])
        ->first();
        if(isset($query)) {
            return response()->json([
                'success' => true,
                'user' => $request['username'],
                'status' => true,
            ],200);
        } else {
            return response()->json([
                'success' => false,
                'status' => false,
                'message' => 'username '.$request['username'].' not found'
            ],400);
        }
    }
    public function CheckBlockUsername(Request $request) 
    {
        // ../api/rad-checkblock
        return $this->blockStatus($request);
    }

    public function UserInfo()
    {
        // ../api/rad-alluser
        $query =  DB::table('userinfo')->get();
        return response()->json([
            'success' => true,
            'user' => $query,
        ],200);
    }

    public function FindByUsername(Request $request)
    {
        // ../api/rad-findusername
        try {
            $query = DB::table('userinfo')->where('username', $request['username'])->firstOrFail();
            return response()->json([
                'success' => true,
                'user' => $query,
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => 'not_found',
                'user' => $request['username'],
                'message' => 'username not found'
            ],422);
        }
    }


    public function usernameCIDMatch(Request $request)
    {
        // ../rad-cid-match/{all (GET), search (POST)}
        // parameters for POST method : {cid: xxxxx..}
        $validation = $this->validatorCID($request);
        if($validation and $request->method() == 'POST') {
            return $validation;
        }else {
            return $this->usernameCID($request); 
        }
    }
    public function BlockUserConnection(Request $request)
    {
        //dd('hai');
        /*
        ../api/rad-block
        ***PROCESS***
        1. /api/rad-block get POST request with CID parameters
        2. CID passed into usernameCIDMatch() to find correlates beetween CID and username
        3. if CID has > 1 username, block abort
        4. else username do status whether already block or not
        5. if status not blocked, do block. else throw error 
        */
        $match = $this->usernameCIDMatch($request);
        if($match->getData()->success == true) {
            if($match->getData()->count > 1){
                return response()->json([
                    'success' => false,
                    'cid' => $request['cid'],
                    'error' => 'CID has more than 1 username',
                    'message' => 'CID has more than 1 username'
                ],422);
            }
            try {
                $user_check = new Request(['username'=> $match->getData()->user[0]->username]);
                $CheckBlock = $this->CheckBlockUsername($user_check);
                if($CheckBlock->getData()->status == true) {
                    return $CheckBlock;
                } 

                //query sebelum migrasi huawei
                $queryUG = DB::table('radusergroup')->insert([
                    'username' => $user_check['username'],
                    'groupname' => ApiController::groupname_blocked,
                    'priority' =>  ApiController::block_priority
                ]);

                //query setelah migrasi huawei
                $queryRep = DB::table('radreply')->insert([
                    'username' => $user_check['username'],
                    'attribute' => ApiController::attribute,
                    'op' =>  ':=',
                    'value' =>  ApiController::value
                ]);

                return response()->json([
                    'success' => true,
                    'status' =>true,
                    'cid' => $request['cid'],
                    'user' => $user_check['username'],
                    'message' => 'Block user successfully'
                ],201);                 
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'cid' => $request['cid'],
                    'message' => 'Error on block process'
                ],400);
            }
        }else {
            return $match;
        }
    }
    public function UnblockUserConnection(Request $request)
    {
        // ../api/rad-unblock

        $match = $this->usernameCIDMatch($request);
        if($match->getData()->success == true) {
            if($match->getData()->count > 1){
                return response()->json([
                    'success' => false,
                    'cid' => $request['cid'],
                    'message' => 'CID has more than 1 username'
                ],422);
            }
            try {
                $user_check = new Request(['username'=> $match->getData()->user[0]->username]);
                $CheckBlock = $this->CheckBlockUsername($user_check);
                if($CheckBlock->getData()->status == false) {
                    return $CheckBlock;
                }

                //query sebelum migrasi HUAWEI
                $queryUG =  DB::table('radusergroup')->where('username', $user_check['username'])
                ->where('groupname', ApiController::groupname_blocked)
                ->where('priority', ApiController::block_priority)
                ->delete();
                
                //query setelah migrasi HUAWEI
                $queryRep =  DB::table('radreply')
                ->where('username', $user_check['username'])
                ->where('attribute', ApiController::attribute)
                ->where('value', ApiController::value)
                ->delete();

                return response()->json([
                    'success' => true,
                    'cid' => $request['cid'],
                    'user' => $user_check['username'],
                    'message' => 'Unblock user successfully'
                ],201);                 
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'cid' => $request['cid'],
                    'message' => 'Error on unblock process'
                ],400);
            }
        }else {
            return $match;
        }
    }
    
    public function groupStatusAll() 
    {
        // ../api/rad-groupstatus/all
        $query = 
        DB::select(DB::raw("
            SELECT distinct(radcheck.username), 
            userinfo.notes as cid,
            radcheck.id,
            radusergroup.groupname as groupname, 
            radcheck.attribute,
            radcheck.value, 
            radreply.attribute as radreply_attribute,
            radreply.value as radreply_value, 
            userinfo.firstname, 
            userinfo.lastname, 
            IFNULL(disabled.username,0) as disabled 
        FROM radcheck
        LEFT JOIN radreply ON radcheck.username=radreply.username
        LEFT JOIN radusergroup ON radcheck.username=radusergroup.username
        LEFT JOIN userinfo ON radcheck.username=userinfo.username
        LEFT JOIN radusergroup disabled ON disabled.username=userinfo.username
            AND disabled.groupname = 'daloRADIUS-Disabled-Users'
            AND disabled.priority = '-999'
        WHERE (radcheck.username=userinfo.username)
            AND radcheck.Attribute IN ('Cleartext-Password', 'Auth-Type','User-Password', 'Crypt-Password', 
                'MD5-Password', 'SMD5-Password', 'SHA-Password', 'SSHA-Password', 'NT-Password', 
                'LM-Password', 'SHA1-Password', 'CHAP-Password', 'NS-MTA-MD5-Password')
            AND userinfo.notes <> ''
        GROUP by radcheck.Username ORDER BY id asc"));

        return response()->json([
            'success' => true,
            'count' => count($query),
            'results' => $query,
        ],200);  
    }

    public function groupStatusSearch(Request $request) 
    {
        // ../api/rad-groupstatus/search

        $query = 
        DB::select(DB::raw("
            SELECT distinct(radcheck.username), 
            userinfo.notes as cid,
            radcheck.id,
            radusergroup.groupname as groupname, 
            radcheck.attribute,
            radcheck.value, 
            radreply.attribute as radreply_attribute,
            radreply.value as radreply_value, 
            userinfo.firstname, 
            userinfo.lastname, 
            IFNULL(disabled.username,0) as disabled
            FROM radcheck
        LEFT JOIN radreply ON radcheck.username=radreply.username
        LEFT JOIN radusergroup ON radcheck.username=radusergroup.username
        LEFT JOIN userinfo ON radcheck.username=userinfo.username
        LEFT JOIN radusergroup disabled ON disabled.username=userinfo.username
            AND disabled.groupname = 'daloRADIUS-Disabled-Users'
            AND disabled.priority = '-999'
        WHERE (radcheck.username=userinfo.username)
            AND userinfo.notes = ?
            AND radcheck.attribute IN ('Cleartext-Password', 'Auth-Type','User-Password', 'Crypt-Password', 
                'MD5-Password', 'SMD5-Password', 'SHA-Password', 'SSHA-Password', 'NT-Password', 
                'LM-Password', 'SHA1-Password', 'CHAP-Password', 'NS-MTA-MD5-Password')
        GROUP by radcheck.Username ORDER BY id asc"), array($request['cid'],));
    
        if(count($query) == 0) {
            return response()->json([
                'success' => false,
                'cid' => $request['cid'],
                'message' => 'CID '.$request['cid'].' not found',
            ],202); 
        } else {
            return response()->json([
                'success' => true,
                'results' => $query,
            ],200); 
        }
    }

    public function fetchUserGroup (Request $request) {
        // ../api/rad-usergroup/{all,search}
    }

    public function dummy(Request $request)
    {
        // return 'aa';
        $query =  RadUserGroup::join('userinfo', 'radusergroup.username', '=', 'userinfo.username')
        ->where('radusergroup.username', $request['username'])
        ->where('radusergroup.groupname', $this->groupname_blocked)
        ->where('radusergroup.priority', $this->block_priority)
        ->firstOrFail(); 
        return $query;
    }

    public function userAdd(Request $request)
    {
        // dd('hai');
        /*
        ***PROCESS***
        1. Check into radcheck, radusergroup, radreply, userinfo by username
        2. Check into userinfo by CID
        3. If exist return user exist
        4. if pass validation insert into radcheck, radusergroup, radreply, userinfo
        5. if success return success message
        */

        //return "hai";

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'cid' => 'required',
            'groupname' => 'required',
            'attribute' => 'required',
            'attribute_value' => 'required',
            'firstname' => 'required',
            'createdby' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => false,
                'error' => $validator->errors(),
                'message' => 'Validation Error',
            ],202);        
        }

        $username=$request->username;
        $password=$request->password;
        $cid=$request->cid;
        $groupname=$request->groupname;
        $attribute=$request->attribute;
        $attribute_value=$request->attribute_value;
        $firstname=$request->firstname;
        $lastname=$request->lastname;
        $createdby=$request->createdby;
        $now=Carbon::now();

        //1. Check into radcheck, radusergroup, radreply, userinfo by username
        $checkUsernameRadcheck=Radcheck::where('username',$username)->count();
        $checkUsernameRadUserGroup=RadUserGroup::where('username',$username)->count();
        $checkUsernameRadReply=RadReply::where('username',$username)->count();
        $checkUsernameUserInfo=Userinfo::where('username',$username)->count();

        //2. Check into userinfo by CID
        $checkCidUserInfo=Userinfo::where('notes',$cid)->count();

        //3. If exist return user exist
        if($checkCidUserInfo > 0 || $checkUsernameRadcheck > 0 || $checkUsernameRadUserGroup > 0 || $checkUsernameRadReply > 0 || $checkUsernameUserInfo > 0){
            return response()->json([
                'success' => false,
                'message' => 'Username or Customer Code Already Exist',
            ],202);               
        }
        else{
            //4. if pass validation insert into radcheck, radusergroup, radreply, userinfo
            DB::beginTransaction();
            try {
                DB::table('radcheck')->insert([
                    [
                        'username' => $username, 
                        'attribute' => 'Cleartext-Password', 
                        'op' => ':=', 
                        'value' => $password
                    ],
                    [
                        'username' => $username, 
                        'attribute' => 'Simultaneous-Use', 
                        'op' => ':=', 
                        'value' => '2'
                    ]
                ]);

                DB::table('radusergroup')->insert([
                    'username' => $username, 
                    'groupname' => $groupname, 
                    'priority' => '0',
                ]);

                DB::table('radreply')->insert([
                    [
                        'username' => $username, 
                        'attribute' =>  $attribute, 
                        'op' => ':=', 
                        'value' => $attribute_value
                    ],
                    [
                        'username' => $username, 
                        'attribute' => 'Session-Timeout', 
                        'op' => ':=', 
                        'value' => '86400'
                    ]
                ]);

                DB::table('userinfo')->insert([
                    'username' => $username, 
                    'firstname' => $firstname, 
                    'lastname' => $lastname, 
                    'notes' => $cid,
                    'creationdate' => $now, 
                    'creationby' => $createdby, 
                    'updatedate' => $now, 
                    'updateby' => $createdby
                ]);

                DB::commit();

                // all good
                return response()->json([
                    'success' => true,
                    'cid' => $cid,
                    'username' => $username,
                    'message' => 'Success Created User Radius'
                ],200);  
            } catch (\Exception $e) {
                DB::rollback();

                // something went wrong
                return response()->json([
                    'success' => false,
                    'cid' => $cid,
                    'username' => $username,
                    'message' => 'Failed Created User Radius'
                ],500);
            }
        }
    }

    public function userDelete(Request $request)
    {
        //return "hai";

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => false,
                'error' => $validator->errors(),
                'message' => 'Validation Error',
            ],202);        
        }

        $username=$request->username;
        // dd($username);

        //1. Check into radcheck, radusergroup, radreply, userinfo by username
        $checkUsernameRadcheck=Radcheck::where('username',$username)->count();
        $checkUsernameRadUserGroup=RadUserGroup::where('username',$username)->count();
        $checkUsernameRadReply=RadReply::where('username',$username)->count();
        $checkUsernameUserInfo=Userinfo::where('username',$username)->count();

        //1. If exist return user not found
        if($checkUsernameRadcheck == '0' || $checkUsernameRadUserGroup == '0' || $checkUsernameRadReply == '0' || $checkUsernameUserInfo == '0'){
            return response()->json([
                'success' => false,
                'message' => 'Username Not Found',
            ],202);               
        }
        else{
            DB::beginTransaction();
            try {
                DB::table('radcheck')->where('username',$username)->delete();

                DB::table('radusergroup')->where('username',$username)->delete();

                DB::table('radreply')->where('username',$username)->delete();

                DB::table('userinfo')->where('username',$username)->delete();

                DB::commit();

                // all good
                return response()->json([
                    'success' => true,
                    'username' => $username,
                    'message' => 'Success Deleted User Radius'
                ],200);  
            } catch (\Exception $e) {
                DB::rollback();

                // something went wrong
                return response()->json([
                    'success' => false,
                    'username' => $username,
                    'message' => 'Failed Deleted User Radius'
                ],500);
            }
        }
    }

    public function CheckRadUsage(Request $request)
    {
        //dd($request->all());
        $query = DB::table('radacct')
        ->select('username', 'framedipaddress', 'acctstarttime', 'acctstoptime', DB::raw('acctsessiontime as "Total Time"'), 'acctinputoctets as Upload', 'acctoutputoctets as Download', 'acctterminatecause', 'nasipaddress')
        ->where('username', $request['username']);

        if($request['period']!=''){
            $query->where(DB::raw('date_format(acctstarttime,"%Y-%m")'), $request['period']);
        }

        $count=$query->count();
        $dataUsages= $query->get();
        if($count > 0){
            return response()->json([
                'success' => true,
                'data' => $dataUsages,
            ],200);
        }
        else{
            return response()->json([
                'success' => false,
                'error' => 'not_found',
                'username' => $request['username'],
                'period' => $request['period'],
                'message' => 'Data Usage Not Found'
            ],422);
        }
    }

    public function SumRadUsage($year)
    {
        //dd(now());
        //$year = "2022";

        $startDate = Carbon::create($year, 1, 1)->startOfYear();
        $endDate = Carbon::create($year, 12, 31)->endOfYear();

        $query = DB::table('radacct')
             ->select(DB::raw("date_format(acctstarttime,'%Y-%m') as period,
                SUM(CASE
                    WHEN date_format(acctstarttime,'%Y-%m')=date_format(acctstarttime,'%Y-%m') 
                    THEN acctsessiontime
                    ELSE 0
                    END) AS sum_total_time,
                SUM(CASE
                    WHEN date_format(acctstarttime,'%Y-%m')=date_format(acctstarttime,'%Y-%m') 
                    THEN acctinputoctets
                    ELSE 0
                    END) AS sum_upload,
                SUM(CASE
                    WHEN date_format(acctstarttime,'%Y-%m')=date_format(acctstarttime,'%Y-%m') 
                    THEN acctoutputoctets
                    ELSE 0
                    END) AS sum_download"))
            ->whereBetween('acctstarttime', [$startDate, $endDate])
            ->groupBy('period')
            ->get();
            
            $period = $query->pluck('period');
            $total_time = $query->pluck('sum_total_time');
            $upload = $query->pluck('sum_upload');
            $download = $query->pluck('sum_download');

            $data = [];
            foreach ($query as $row) {
                $data[] = [
                    'period' => $row->period,
                    'sum_total_time' => $row->sum_total_time,
                    'sum_upload' => $row->sum_upload,
                    'sum_download' => $row->sum_download,
                ];
            }

            $response = [
                'success' => true,
                'data' => $data,
            ];

            return response()->json($response,200);
    }
}